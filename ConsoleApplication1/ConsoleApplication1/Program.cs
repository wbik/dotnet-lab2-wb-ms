﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
     public class Program
    {
        public char[,] plansza_zycia = {{'*','*','*','.','.'},{'*','.','*','.','.'},{'.','*','.','.','*'},{'*','.','.','*','.'},{'.','.','*','*','.'}};

        bool czy_istnieje(int i, int j)
        {
            if (i < 0 || i > 4 || j < 0 || j > 4)
                return false;
            else 
                return true;
        }
        int sprawdzenie_pol_czy_zyje(int i, int j)
        {
            int ile = 0;
            if(czy_istnieje(i+1,j))
            if (plansza_zycia[i+1, j] == '*')
                ile++;
            if(czy_istnieje(i,j+1))
            if (plansza_zycia[i, j+1] == '*')
                ile++;
            if (czy_istnieje(i-1, j))
            if (plansza_zycia[i-1, j] == '*')
                ile++;
            if (czy_istnieje(i, j-1))
            if (plansza_zycia[i, j-1] == '*')
                ile++;
            if (czy_istnieje(i+1, j+1))
            if (plansza_zycia[i+1, j+1] == '*')
                ile++;
            if (czy_istnieje(i-1, j-1))
            if (plansza_zycia[i-1, j-1] == '*')
                ile++;
            if (czy_istnieje(i+1, j-1))
            if (plansza_zycia[i+1, j-1] == '*')
                ile++;
            if (czy_istnieje(i-1, j+1))
            if (plansza_zycia[i-1, j+1] == '*')
                ile++;
            return ile;
        }

       public void akcja_pola()
        {
            char[,] plansza_zycia_kopia = plansza_zycia;
            for (int i = 0; i < 5; i++)
                for (int j = 0; j < 5; j++)
                {
                    if (sprawdzenie_pol_czy_zyje(i, j) < 2 || sprawdzenie_pol_czy_zyje(i, j) > 3)
                        plansza_zycia_kopia[i, j] = '.';
                    if (sprawdzenie_pol_czy_zyje(i, j) == 3)
                        plansza_zycia_kopia[i, j] = '*';
                }
            plansza_zycia = plansza_zycia_kopia;

        }
        }

        class testProgram
        {
            static void Main()
            {
                Program var = new Program();
                while(true)
                {
                    Console.Clear();
                    for (int i = 0; i < 5; i++)
                    {
                        Console.WriteLine();
                        for (int j = 0; j < 5; j++)
                            Console.Write(var.plansza_zycia[i, j]);
                    }
                var.akcja_pola();
                Console.Read();
                }
            }
        }
    }